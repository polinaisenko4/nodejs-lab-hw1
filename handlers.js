const {filePath} = require('./config')
const Database = require('./database');
const path = require("path");

const db = new Database(filePath);

const createFile = async (req, res) => {

    const filename = req.body.filename;
    const content = req.body.content;
    if (!filename || content === undefined) {
        return res.status(400).json({
            message: "Please specify 'content' parameter"
        })
    }
    try {
        await db.createFile(filename, content);
        res.status(200).json({
            message: "File created successfully"
        });
    } catch (err) {
        res.status(400).json({
            message: "Please specify 'content' parameter"
        });
    }
};

const getFile = async (req, res) => {
    const filename = req.params.filename;
    try {
        const content = await db.readFile(filename);
        const {ctime: time} = await db.getStats(filename);
        res.status(200).json({
            message: "Success",
            filename,
            content,
            extension: path.extname(filename).slice(1),
            uploadedDate: time.toISOString()
        });
    } catch (err) {
        res.status(400).json({
            message: `No file with ${filename} filename found`
        });
    }
}

const getFiles = async (req, res) => {
    try {
        const files = await db.readFiles()
        res.status(200).json({
            message: "Success",
            files: [...files]
        });
    } catch (err) {
        res.status(400).json({
            message: "Client error"
        });
    }
}

module.exports = {
    createFile,
    getFile,
    getFiles
}