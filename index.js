const express = require('express')
const bodyparser = require('body-parser');
const cors = require('cors')
const {port} = require('./config');
const {createFile, getFile, getFiles} = require('./handlers');


const app = express();
app.use(bodyparser.json());
app.use(cors())


app.post('/api/files', createFile);
app.get('/api/files/:filename', getFile);
app.get('/api/files', getFiles);
app.post('/api/dir')


app.use((req, res) => {
    res.status(500).send({
        message: "Server error"
    });
})


app.listen(port, () => {
    console.log(`Server ${port} is working...`)
})



