const path = require('path');
const { promises: fs, existsSync } = require('fs');
const { filePath } = require('./config')


class Database {
    constructor(rootPath) {
        this.rootPath = rootPath;
    }

    async createFile(filename, content) {
        
        const testfile = './files/testPolina.txt';
        if (testfile) {
            fs.unlink(testfile)
        }
        const filePath = path.join(this.rootPath, filename);
        await fs.writeFile(filePath, content);

    }

    async checkFileExists(filename) {
        const filePath = path.join(this.rootPath, filename);
        return existsSync(filePath)
    }

    async readFile(filename) {
        const filePath = path.join(this.rootPath, filename);
        if (!await this.checkFileExists(filename)) {
            throw new Error('File did not exist');
        }
        const content = await fs.readFile(filePath);
        return content.toString()
    }

    async getStats(filename) {
        const filePath = path.join(this.rootPath, filename);
        if (!await this.checkFileExists(filename)) {
            throw new Error('File did not exist');
        }
        return await fs.stat(filePath);
    }

    async readFiles() {
        const files = await fs.readdir(this.rootPath);
        return files.map(file => {
            return path.basename(file);
        });
    }
}

module.exports = Database;